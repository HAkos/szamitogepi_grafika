#include "line.h"
#include <SDL2/SDL.h>
#include <stdbool.h>

line_t *lines;

int main()
{
	SDL_Init(SDL_INIT_VIDEO);

	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;

	window = SDL_CreateWindow("Lines", SDL_WINDOWPOS_CENTERED,
						 SDL_WINDOWPOS_CENTERED, 1280, 720, 0);
	if (window == NULL)
		exit(1);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == NULL)
		exit(1);

	bool quit = false;
	SDL_Event e;

	lines = (line_t*) malloc(MAX_LINE_COUNT*sizeof(line_t));
	int line_num = 0;
	int clicks = 0;

	SDL_SetRenderDrawColor(renderer, 80, 80, 80, 255);
	SDL_RenderClear(renderer);
	while (!quit)
	{
		while(SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				quit = true;
			if (e.type == SDL_MOUSEBUTTONUP && e.button.button == SDL_BUTTON_LEFT)
			{
				clicks++;
				if (clicks % 2 != 0)
				{
					lines[line_num].startx = e.button.x;
					lines[line_num].starty = e.button.y;
				}
				else
				{
					lines[line_num].endx = e.button.x;
					lines[line_num].endy = e.button.y;
					color_t c = {250, 0, 0};
					lines[line_num].color = c;
					line_t l = lines[line_num];
					SDL_SetRenderDrawColor(renderer, l.color.r, l.color.g, l.color.g, 255);
					SDL_RenderDrawLine(renderer, l.startx, l.starty, l.endx, l.endy);
					line_num++;
				}
			}
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(10);
	}

	return 0;
}

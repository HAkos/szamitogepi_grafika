#define MAX_LINE_COUNT 1000

typedef struct color {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} color_t;

typedef struct line {
	int startx;
	int starty;
	int endx;
	int endy;
	color_t color;
} line_t;
